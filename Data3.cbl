       IDENTIFICATION DIVISION. 
       PROGRAM-ID. DATA3.
       AUTHOR. WORAMET.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  SURNAME           PIC   X(8) VALUE "COUGHLAN".
       01  SALE-PRICE        PIC   9(4)V99.
       01  NUM-OF-EMPLOYEE   PIC   999V99.
       01  SALARY            PIC   9999V99.
       01  COUNTY-NAME       PIC   X(9).

       PROCEDURE DIVISION.
       Begin.
           DISPLAY "1 "   SURNAME 
           MOVE "SMITH"   TO SURNAME 
           DISPLAY "2 "   SURNAME 
           MOVE "FITZWILLIAM"   TO SURNAME 
           DISPLAY "3 "   SURNAME 
           .
           DISPLAY "1 "   SALE-PRICE 
           MOVE  ZEROS TO SALE-PRICE 
           DISPLAY "2 "   SALE-PRICE 
           MOVE  25.5  TO SALE-PRICE 
           DISPLAY "3 "   SALE-PRICE 
      *     01  SALE-PRICE     PIC   9(4)V99. 0007.55
           MOVE  7.553  TO SALE-PRICE 
           DISPLAY "4 "   SALE-PRICE 
      *     01  SALE-PRICE     PIC   9(4)V99. 3425.15
           MOVE  93425.158  TO SALE-PRICE 
           DISPLAY "5 "   SALE-PRICE 
      *     01  SALE-PRICE     PIC   9(4)V99. 0128.00
           MOVE  128  TO SALE-PRICE 
           DISPLAY "6 "   SALE-PRICE      
           .
           DISPLAY "1 " NUM-OF-EMPLOYEE
      *     01  NUM-OF-EMPLOYEE   PIC   999V99. 012.40
           MOVE  12.4  TO NUM-OF-EMPLOYEE 
           DISPLAY "2 " NUM-OF-EMPLOYEE 
      *     01  NUM-OF-EMPLOYEE   PIC   999V99. 745.00
           MOVE  6745  TO NUM-OF-EMPLOYEE 
           DISPLAY "3 " NUM-OF-EMPLOYEE 
           . 
           DISPLAY "1 " SALARY 
      *     01  SALARY            PIC   9999V99.  0745.00
           MOVE NUM-OF-EMPLOYEE TO SALARY 
           DISPLAY "2 "   SALARY 
           .
           DISPLAY "1 " COUNTY-NAME 
           MOVE "GALWAY"  TO COUNTY-NAME 
           DISPLAY "2 " COUNTY-NAME 
           MOVE ALL  "@-$"  TO COUNTY-NAME 
           DISPLAY "3 " COUNTY-NAME 
           .
